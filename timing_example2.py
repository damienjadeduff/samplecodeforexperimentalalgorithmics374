import timeit
import time

def reverse_list_loop(l):
    a = []
    for x in range(len(l)-1,-1,-1):
        a.append(x)
    return a

def reverse_list_comprehension(l):
    a= [l[n] for n in range(len(l)-1,-1,-1)]
    return a

def test_reverse_list_approach_loop(l):
    a = reverse_list_loop(l)
    return

def test_reverse_list_approach_comprehension(l):
    a = reverse_list_comprehension(l)
    return

def timefunc(func,data,timer,iters):
    res = timeit.timeit('thefunction(thedata)', timer=timer, globals={'thefunction':func,'thedata':data}, number=iters)
    return res
    
if __name__=='__main__':

    datatypes =     [
                    ('ints',int),
                    ('strs',str)
                ]
    functions = [
                    ('loop',test_reverse_list_approach_loop),
                    ('comprehension',test_reverse_list_approach_comprehension)
                ]
    timers =    [
                    ('time',time.time),
                    ('process_time',time.process_time),
                    ('perf_counter',time.perf_counter)
                ]

    lengths = range(1000,50000,200)

    num_samples = 1
    
    with open('out2.csv','w') as f:

        overall_cntr = 0                    
        print('datatype,function,timer,length,result',file=f)
        for tname,tval in timers:
            for dname,datatype in datatypes:
                for fname,fval in functions:
                    print(dname+','+fname+','+tname)
                    for length in lengths:

                        data = [datatype(s) for s in range(0,length)]

                        for count in range(0,num_samples):
                            if overall_cntr % 50 == 0:
                                print("Progress:",overall_cntr)
                                print(dname+','+fname+','+tname+','+str(length))
                            overall_cntr += 1
                            res = timefunc(fval,data,tval,100)
                            print(dname+','+fname+','+tname+','+str(length)+','+str(res),file=f)
