SAMPLE CODE FOR EXPERIMENT WITH ALGORITHM PERFORMANCE
=====================================================

The aim of this small repository is to give an idea of how one might measure the performance of an algorithm. Below is a description of each file and what it might contribute to you.

timing_example.py
-----------------

In the timing_example.py file there are two functions that reverse a list:

* ``reverse_list_loop`` which reverses a list (a Python style vector/array) using a for loop
* ``reverse_list_comprehension`` which reverses a list (a Python style vector/array) using a special Python feature called a list comprehension

The details of their implementation are unimportant.

The important thing is that this gives one example how they may be compared.

The functions ``test_reverse_list_approach_loop`` and ``test_reverse_list_approach_comprehension`` just call the functions and throw away the output. They are not really necessary here but in some cases some kind of intermediate test function like this may be necessary.

The function ``timefunc`` uses the Python ``timeit`` library to time a given function by calling it on given data and using a given timer. It is also provided with the number of times to call this function.

In the main loop, maps (in Python, dicts) are set up containing all the possible:

* input data (a list of either 1000 strings or 1000 ints).
* functions to run on that data (either the ``test_reverse_list_approach_loop`` function or the ``test_reverse_list_approach_comprehension`` function).
* the different timers to run on them (the normal Python time, a process time which tries to ignore time outside of the process, and a performance timer - see the ``timeit`` module for more details of these different timers - you would generally only want to use one of these for your project).

For each combination of these, the ``timefunc`` function is called 100 times. Those 100 times result in 100 performance numbers (times). These are all written to a CSV file called ``out.csv``

out.csv
-------

This is just the output of ``timing_example.py`` after I ran it. You could regenerate this file on your computer by running

    python3 timing_example.py

out.ods
-------

This is the result of doing some spreadsheet analysis on ``out.csv``.

``out.csv`` is loaded into a spreadsheet workbook and the data is left in the ``Data`` worksheet.

The ``Pivot_Table_Mean`` and ``Pivot_Table_StdDev`` worksheets are the result of using the "Pivot Table" spreadsheet function. You can see some interesting results in those tables. You should try it yourself.

``Scatter_Plot_Str_ListVsFor`` and ``Scatter_Loop_For_StrVsInt`` are examples of a simple scatter plot comparing the ``reverse_list_loop`` and ``reverse_list_comprehension`` approaches on strings using the Process Time measure, in the first case, and comparing the effect of int vs string data types with for loops in the second. Obviously there are other analyses that can be done. You should try it yourself.

timing_example2.py
------------------

A version of ``timing_example.py`` where array size is also varied.

out2.csv
-------

The output of ``timing_example2.py``.

out2.ods
--------

An example of a simple analysis on the data from ``out2.csv``.

An interesting artifact can be observed in the data that is probably due to some system process or Python-internal process. To deal with these systematic artifacts, one approach is to randomise things more (e.g. the order in which conditions are trialed).